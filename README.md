# openpandora-debian

WIFI Working, 
Xorg Working,
USB Working,
Audio Working,
... 

Method, debootstrap onto your SD

Create a ext2 /dev/mmcblk0p1

Add autoboot.txt file.

Start the scripts to get wifi. 

Have fun!


![](image/debian.png)

Status: 2014

(Upgrade to systemd? likely devuan... )



# Installation Method Debian 

Enabled in in debian linux package in 4.7.4-1.

Boots with linux 4.7.x/4.8.x and Debian stretch. SD card, NAND, LCD (xserver-xorg-video-fbdev works, fbconsole doesn't), keyboard (fn-key broken). Broken or missing support for USB, wifi, and "mouse" nubs.

U-boot
The omap3_pandora target was enabled in the u-boot-omap package in version 2016.09~rc2+dfsg1-1. Only works with serial console, so debugging can be difficult without a serial console cable.

Installing Wheezy on the Open Pandora
What You will need
An Open Pandora (I have the 1 Ghz Model)
- Note: I have been told this can make a different for which port you can use. I have been able to use armhf, but I wouldn't mind someone trying it out....
A PC with Debian installed
An SD Card (I am using a 16 GB card, but I imagine that you can install it on an 8 GB Card)
Notes
As of now, 3D acceleration does not work. I cannot get the omap drivers to work
I have only been able to get the pandora kernel to play nicely with Debian.
I am NOT trying to get the PND system to work. You are welcome to attempt it, and there is some documention here: http://pandorawiki.org/Debian_On_SD.

The Wifi Drivers are non-free software.
On The PC
Format the SD card to have an ext3 partition (I used gparted to do this)
Install debootstrap
# apt-get install debootstrap
Now run debootstrap. I choose to use the armel port, even though Open Pandora has a hardware FPU. Others have successfully gotten the armel working, so I will do that then work on the armhf
#debootstrap --arch armel --foreign wheezy /tmp/pandora-debian http://ftp.us.debian.org/debian/
Note: If you want to try the armhf port, run this instead:
#debootstrap --arch armhf --foreign wheezy /tmp/pandora-debian http://ftp.us.debian.org/debian/
Now copy that directory you just made into the SD card, unmount it, and put it into your Pandora.
# cd /tmp/pandora-debian
# cp -R -v * /media/chris/Debian/ 
# umount /media/chris/Debian/
On The Pandora
Now on your Pandora, you will have to do the second stage. This will take a little bit of time:
# sudo chroot /media/Debian
# /debootstrap/deboostrap --second-stage
After that, you need to add your repositories into this new install.
# nano /etc/apt/sources.list
Contents of /etc/apt/sources.list

# wheezy
deb http://ftp.us.debian.org/debian/ wheezy main contrib non-free
deb-src http://ftp.us.debian.org/debian/ wheezy main contrib

# wheezy updates
deb http://ftp.us.debian.org/debian/ wheezy-updates main contrib non-free
deb-src http://ftp.us.debian.org/debian/ wheezy-updates main contrib

# Wheezy Backports
deb http://ftp.us.debian.org/debian/ wheezy-backports main contrib non-free
deb-src http://ftp.us.debian.org/debian/ wheezy-backports main contrib
Now update and upgrade. I don't think you will get anything for the upgrade, but it doesn't hurt.
# apt-get update
# apt-get dist-upgrade
Making the Pandora boot
You must also create a boot.txt or autoboot.txt in order for u-boot to recognize and boot the kernel. If you make a boot.txt, you must manually load it from the bootmenu, but autoboot.txt will boot this before the NAND image:
# nano /boot.txt
or

# nano /autoboot.txt
Contents of boot.txt/autoboot.txt:

setenv bootargs root=/dev/mmcblk0p1 rw rootwait vram=6272K omapfb.vram=0:3000K mmc_core.removable=0
ext2load mmc 0:1 0x80300000 /boot/uImage
bootm 0x80300000
NOTE: Refer to boot setup for how I did this. If you make multiple partitions on the card, the ezt2load will be different: http://pandorawiki.org/Boot_setup

Using the Pandora Kernel
From Angstrom:

$ sudo cp -R /lib/modules/* /media/Debian/lib/modules
$ sudo cp -R /lib/firmware/* /media/Debian/lib/firmware
$ sudo cp -R /boot/* /media/Debian/boot
Chroot with Debian:

# chown root:root -hR /boot/
# chown root:root -hR /lib/firmware/
# chown root:root -hR /lib/modules/
With that, you can now boot from the pandora directly to your new Debian installation!

Making a Debian Kernel
NOTE: This is only possible with armhf. However, upon boot, you will only see a blank screen. I suspect a kernel recompile is needed for both armel and armhf.
Install the kernel from apt-get (NOTE: The firmware-ti-connectivity is non-free):
On Debian:

# apt-get install linux-image-omap linux-headers-omap firmware-ti-connectivity
Now to make the pandora boot into it, we must make a bootable image for it to boot from.

On Debian:

# mkimage -A arm -O linux -T kernel -C none -a 0x80008000 -e 0x80008000 -n Linux -d /boot/vmlinuz-3.2.0-4-omap /boot/uImage
As of now, it says it boots from the Uboot, but you will see nothing on the screen. To work on.....
Xorg
When you install xxerver, be sure to install xserver-xorg-video-omap3 and xserver-xorg-video-fbdev. I chose to install xfce, but you are welcome to install what you wish.

On Debian

# apt-get install xfce4 xserver-xorg-video-omap3 xserver-xorg-video-fbdev
I also install xfce4-goodies

# apt-get install xfce4-goodies
=== armel===

This is my xorg.conf

/etc/X11/xorg.conf

Section "Module"
    Load    "extmod"
    Load    "dbe"
    Disable    "glx"
    Disable    "dri"
    Load    "dri2"
EndSection

Section "ServerLayout"
    Identifier    "DefaultLayout"
    Screen        "Screen0"
EndSection

Section "Screen"
    Identifier        "Screen0"
    Device            "OMAPFB"
    Monitor            "Monitor0"
    DefaultDepth    16
    SubSection "Display"
        Depth    16
        Modes    "800x480"
    EndSubSection
EndSection

Section "Monitor"
    Identifier        "Monitor0"
EndSection

Section "Device"
    Identifier        "OMAPFB"
    Driver            "omapfb"
    Option            "fb"        "/dev/fb0"
EndSection
Right now I have only attempted with root.

armhf
Note: I have only gotten fbdev to work. omap3 does not seem to want to work.

This is my xorg.conf

/etc/X11/xorg.conf

# Taken from Pandian
Section "ServerLayout"
        Identifier      "DefaultLayout"
        Screen          "Screen0"
EndSection

Section "Screen"
        Identifier      "Screen0"
        Device          "FBDEV"
        Monitor         "Monitor0"
        DefaultDepth    16
        SubSection "Display"
                Depth   16
                Modes   "800x480"
        EndSubSection
EndSection



Section "Device"
        Identifier      "OMAPFB"
        Driver          "omapfb"
        Option          "fb"    "/dev/fb0"
EndSection

Section "Device"
        Identifier      "FBDEV"
        Driver          "fbdev"
        Option          "ShadowFB"              "false"
        Option          "fbdev"                 "/dev/fb0"
EndSection   

Section "Files"
        FontPath        "/usr/share/fonts/X11/misc"
        FontPath        "/usr/share/fonts/X11/Type1"
        FontPath        "/usr/share/fonts/X11/Speedo"
        FontPath        "/usr/share/fonts/X11/100dpi"
        FontPath        "/usr/share/fonts/X11/75dpi"
EndSection
Keymap
On Angstrom:

sudo cp /etc/keymap-extension-2.6.map /media/mmcblk0p1/etc
sudo cp /etc/skel/.pndXmodmap /media/mmcblk0p1/etc/skel
This script needs to run anytime you start X, right now I have it in /etc/skel/.xinitrc. Depending on if you use gdm, kdm, etc, it will have to go somewhere else

.xinitrc

xmodmap /etc/skel/.pndXmodmap
loadkeys /etc/keymap-extension-2.6.map
Battery Monitoring
Nothing really to it....

Debian

# apt-get install apmd
Also install the DM's power manager. I use xfce, so I just installed that.

Credits:

http://pandorawiki.org/Debian_On_SD

http://pandorawiki.org/Boot_setup

http://pandorawiki.org/Firmware

 

